package com.aequus.notification.config

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.scheduling.annotation.EnableAsync
import software.amazon.awssdk.services.sns.SnsClient

@Configuration
@EnableAsync
class SnsConfiguration() {

    @Bean
    fun getSnsClient(): SnsClient {
        return SnsClient.builder().build()
    }

}