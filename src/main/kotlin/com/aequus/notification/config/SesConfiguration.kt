package com.aequus.notification.config

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.scheduling.annotation.EnableAsync
import software.amazon.awssdk.services.ses.SesClient

@Configuration
@EnableAsync
class SesConfiguration() {

    @Bean
    fun getSesClient(): SesClient {
        return SesClient.builder().build()
    }

}