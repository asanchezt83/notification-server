package com.aequus.notification.response

import io.swagger.v3.oas.annotations.media.Schema

data class PublishSnsResponse (
    @Schema(example = "This is a reply message when sending the SMS.")
    val message: String
)