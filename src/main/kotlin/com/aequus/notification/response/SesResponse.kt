package com.aequus.notification.response

import io.swagger.v3.oas.annotations.media.Schema
import java.util.*

data class SendEmailSesResponse(
    @Schema(example = "Reply message when sending mail.")
    val message: String
)

data class SendTemplatedEmailSesResponse(
    @Schema(example = "Reply message when sending mail with template.")
    val message: String
)

data class CreateTemplateSesResponse(
    @Schema(example = "Response message when creating template.")
    val message: String
)

data class ListTemplatesSesResponse(
    @Schema(example = "Response message when list templates.")
    val templates: List<SesTemplateMetadata>
)

data class SesTemplateMetadata(
    @Schema(example = "Name of the template")
    val name: String,
    @Schema(example = "Creation date of the template")
    val creation: Date
)

data class DeleteTemplateSesResponse(
    @Schema(example = "Reply message when deleting template.")
    val message: String
)

data class UpdateTemplateSesResponse(
    @Schema(example = "Response message when updating template.")
    val message: String
)