package com.aequus.notification.controller

import com.aequus.notification.request.*
import com.aequus.notification.response.*
import com.aequus.notification.service.SesService
import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.tags.Tag
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("api/aws/ses")
@Tag(name = "ses", description = "Aws SES API")
class SesController(val sesService: SesService) {

    @Operation(summary = "Send Email")
    @PostMapping("sendEmail")
    fun sendEmail(@RequestBody request: SendEmailSesRequest): SendEmailSesResponse {
        return sesService.sendEmail(request)
    }

    @Operation(summary = "Send Email with template")
    @PostMapping("sendEmailTemplated")
    fun sendTemplatedEmail(@RequestBody request: SendTemplatedEmailSesRequest): SendTemplatedEmailSesResponse {
        return sesService.sendTemplatedEmail(request)
    }

    @Operation(summary = "Create a new template in the SES Service")
    @PostMapping("template/create")
    fun createTemplate(@RequestBody request: CreateTemplateSesRequest): CreateTemplateSesResponse {
        return sesService.createTemplate(request)
    }

    @Operation(summary = "List templates in the SES Service")
    @GetMapping("template/list")
    fun listTemplate(): ListTemplatesSesResponse {
        return sesService.listTemplate()
    }

    @Operation(summary = "Delete the template with AWS")
    @DeleteMapping("template/delete")
    fun deleteTemplate(@RequestBody request: DeleteTemplateSesRequest): DeleteTemplateSesResponse {
        return sesService.deleteTemplate(request)
    }

    @Operation(summary = "Update the template with AWS")
    @PutMapping("template/update")
    fun updateTemplate(@RequestBody request: UpdateTemplateSesRequest): UpdateTemplateSesResponse {
        return sesService.updateTemplate(request)
    }
}