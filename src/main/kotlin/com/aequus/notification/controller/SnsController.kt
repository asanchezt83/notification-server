package com.aequus.notification.controller

import com.aequus.notification.request.PublishSnsRequest
import com.aequus.notification.response.PublishSnsResponse
import com.aequus.notification.service.SnsService
import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.tags.Tag
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("api/aws/sns")
@Tag(name = "sns", description = "Aws SNS API")
class SnsController(val snsService: SnsService) {

    @Operation(summary = "Send Sms", description = "Sends an sms via AWS SNS Service")
    @PostMapping("sendSms")
    fun sendSms(@RequestBody request: PublishSnsRequest): PublishSnsResponse {
        return snsService.sendSms(request)
    }

}