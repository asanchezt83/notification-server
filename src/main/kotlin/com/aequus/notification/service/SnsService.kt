package com.aequus.notification.service

import com.aequus.notification.loggerFor
import com.aequus.notification.request.PublishSnsRequest
import com.aequus.notification.response.PublishSnsResponse
import org.springframework.stereotype.Service
import software.amazon.awssdk.services.sns.SnsClient
import software.amazon.awssdk.services.sns.model.PublishRequest
import net.logstash.logback.argument.StructuredArguments.kv

@Service
class SnsService(private val snsClient: SnsClient) {

    private val logger = loggerFor<SnsService>()

    fun sendSms(request: PublishSnsRequest) : PublishSnsResponse {
        val publishRequest = PublishRequest.builder()
            .message(request.message)
            .phoneNumber(request.phoneNumber)
            .build()

        val result = snsClient.publish(publishRequest)
        logger.info("Sms sent with code: [{}]", kv("messageId", result.messageId()))
        return PublishSnsResponse("Sms sent with code: ["+result.messageId()+"]")
    }
}
