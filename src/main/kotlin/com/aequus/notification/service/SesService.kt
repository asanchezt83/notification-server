package com.aequus.notification.service

import com.aequus.notification.loggerFor
import com.aequus.notification.request.*
import com.aequus.notification.response.*
import com.fasterxml.jackson.databind.ObjectMapper
import net.logstash.logback.argument.StructuredArguments
import org.springframework.stereotype.Service
import software.amazon.awssdk.services.ses.SesClient
import software.amazon.awssdk.services.ses.model.*
import java.util.*

@Service
class SesService(private val client: SesClient) {

    private val log = loggerFor<SesService>()

    fun sendEmail(request: SendEmailSesRequest): SendEmailSesResponse {
        val destination = Destination.builder().toAddresses(request.mailTo).build()

        val message = Message.builder()
            .subject(Content.builder().data(request.subject).build())
            .body(Body.builder().html(Content.builder().data(request.html).build()).build())
            .build()

        val emailRequest = SendEmailRequest.builder()
            .destination(destination)
            .source(request.mailFrom)
            .message(message)
            .build()

        val result = client.sendEmail(emailRequest)
        log.info("Email send with id: [{}]", StructuredArguments.kv("id", result.messageId()))
        return SendEmailSesResponse("Email send with id: ["+result.messageId()+"]")
    }

    fun sendTemplatedEmail(request: SendTemplatedEmailSesRequest): SendTemplatedEmailSesResponse {
        val destination = Destination.builder().toAddresses(request.mailTo).build()
        val templateData = ObjectMapper().writeValueAsString(request.extraFields)

        val emailRequest = SendTemplatedEmailRequest.builder()
            .destination(destination)
            .source(request.mailFrom)
            .template(request.templateName)
            .templateData(templateData)
            .build()

        val result = client.sendTemplatedEmail(emailRequest)
        log.info("EmailTemplated send with id: [{}]", StructuredArguments.kv("id", result.messageId()))
        return SendTemplatedEmailSesResponse("EmailTemplated send with id: ["+result.messageId()+"]")
    }

    fun createTemplate(request: CreateTemplateSesRequest): CreateTemplateSesResponse {
        val templated = Template.builder()
            .templateName(request.templateName)
            .subjectPart(request.subject)
            .textPart(request.data)
            .htmlPart(request.body)
            .build()

        val createTemplateRequest = CreateTemplateRequest.builder()
            .template(templated)
            .build()

        client.createTemplate(createTemplateRequest)
        log.info("The template has been created correctly")
        return CreateTemplateSesResponse("The template ${request.templateName} has been created correctly")
    }

    fun listTemplate(): ListTemplatesSesResponse {
        val listTemplateRequest = ListTemplatesRequest.builder()
            .maxItems(10)
            .nextToken("")
            .build()
        val sesResponse = client.listTemplates(listTemplateRequest)
        return if (sesResponse.hasTemplatesMetadata()){
            log.info("List templates: {}", StructuredArguments.kv("listTemplates", sesResponse.templatesMetadata()))
            ListTemplatesSesResponse( sesResponse.templatesMetadata().map {
                SesTemplateMetadata(it.name(), Date.from(it.createdTimestamp()))
            })
        }
        else
            ListTemplatesSesResponse (emptyList())
    }

    fun deleteTemplate(request: DeleteTemplateSesRequest): DeleteTemplateSesResponse {
        val deleteTemplateRequest = DeleteTemplateRequest.builder()
            .templateName(request.templateName)
            .build()

        client.deleteTemplate(deleteTemplateRequest)
        log.info("The template has been removed successfully")
        return DeleteTemplateSesResponse("The template ${request.templateName} has been removed successfully")
    }

    fun updateTemplate(request: UpdateTemplateSesRequest): UpdateTemplateSesResponse {
        val template = Template.builder()
            .templateName(request.templateName)
            .subjectPart(request.subject)
            .textPart(request.data)
            .htmlPart(request.body)
            .build()

        val updateTemplateRequest = UpdateTemplateRequest.builder()
            .template(template)
            .build()

        client.updateTemplate(updateTemplateRequest)
        log.info("Template updated successfully")
        return UpdateTemplateSesResponse("Template ${request.templateName} updated successfully")

    }

}
