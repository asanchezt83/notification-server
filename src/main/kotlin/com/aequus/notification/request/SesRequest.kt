package com.aequus.notification.request

import io.swagger.v3.oas.annotations.media.Schema

data class SendEmailSesRequest(
    @Schema(example = "from@example.com")
    val mailFrom: String,
    @Schema(example = "from@example.com")
    val mailTo: String,
    @Schema(example = "<h1>This an HTML Test Request</h1>")
    val html: String,
    @Schema(example = "Test Subject")
    val subject: String
)

data class SendTemplatedEmailSesRequest(
    @Schema(example = "from@example.com")
    val mailFrom: String,
    @Schema(example = "from@example.com")
    val mailTo: String,
    @Schema(example = "TemplateTest")
    var templateName: String,
    @Schema(example = "{parameter1:value1,parameter2:value2}")
    val extraFields: Map<String, String>?
)

data class CreateTemplateSesRequest(
    @Schema(example = "Test Subject")
    val subject: String,
    @Schema(example = "{parameter1, parameter2}")
    val data: String,
    @Schema(example = "<h1>This an HTML Test Created. This is my variables: {{parameter1}} , {{parameter2}}</h1>")
    val body: String,
    @Schema(example = "TemplateTestCreated")
    var templateName: String
)

data class DeleteTemplateSesRequest(
    @Schema(example = "TemplateTest")
    val templateName: String
)

data class UpdateTemplateSesRequest(
    @Schema(example = "from@example.com")
    val subject: String,
    @Schema(example = "{parameter1, parameter2}")
    val data: String,
    @Schema(example = "<h1>This an HTML Test Updated. This is my updated variables: {{parameter1}} , {{parameter2}}</h1>")
    val body: String,
    @Schema(example = "TemplateTestUpdated")
    var templateName: String
)