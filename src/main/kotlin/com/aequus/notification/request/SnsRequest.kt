package com.aequus.notification.request

import io.swagger.v3.oas.annotations.media.Schema

data class PublishSnsRequest(
    @Schema(example = "This a message test")
    val message: String,
    @Schema(example = "+16509249204")
    val phoneNumber: String
)