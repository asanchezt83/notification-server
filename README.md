# Aequus Notification Server

### Scope:
Deploy as Internal Service

### Swagger Endpoint:
http://127.0.0.1:3600/openapi/swagger-ui

### Developer Credentials Setup:
Generate credentials via "aws configure" command
```
[default]
aws_access_key_id=XXXXXXXXXXXXXX
aws_secret_access_key=XXXXXXXXXXXX
aws_region=eu-central-1
aws_default_region=eu-central-1
```

Add role at profile config
```
[profile default]
region=eu-central-1
output=json

[profile aequus_role]
output=json
region=eu-central-1
source_profile=default
role_arn=arn:aws:iam::999999999999:role/eks/aequus_staging
```


### Deploy at EKS Cluster:
Generate the follow env variables
```
AWS_DEFAULT_REGION=eu-central-1
AWS_WEB_IDENTITY_TOKEN_FILE=/var/run/secrets/eks.amazonaws.com/serviceaccount/token
AWS_REGION=eu-central-1
AWS_ROLE_ARN=arn:aws:iam::999999999999:role/eks/aequus_staging
```

### Build Docker Image:
gradlew  bootBuildImage