import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
	id("org.springframework.boot") version "2.6.2"
	id("io.spring.dependency-management") version "1.0.11.RELEASE"
	kotlin("jvm") version "1.6.10"
	kotlin("plugin.spring") version "1.6.10"
}

group = "notification-server"
version = "0.0.1-SNAPSHOT"
java.sourceCompatibility = JavaVersion.VERSION_11

repositories {
	mavenCentral()
	maven("https://repo.spring.io/milestone")  // Spring milestones
	maven("https://repo.spring.io/snapshot")   // Spring snapshots
}

dependencies {
	implementation("org.springframework.boot:spring-boot-starter")
	implementation("org.jetbrains.kotlin:kotlin-reflect")
	implementation("org.springframework.boot:spring-boot-devtools:2.6.1")
	implementation("org.springframework.boot:spring-boot-starter-web:2.6.1")
	implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
	implementation("software.amazon.awssdk:sns:2.17.107")
	implementation("software.amazon.awssdk:ses:2.17.107")
	implementation("software.amazon.awssdk:sts:2.17.107")


	// https://mvnrepository.com/artifact/com.fasterxml.jackson.core/jackson-core
	implementation("com.fasterxml.jackson.core:jackson-core:2.13.1")

	// https://mvnrepository.com/artifact/net.logstash.logback/logstash-logback-encoder
	implementation("net.logstash.logback:logstash-logback-encoder:7.0.1")

	//https://mvnrepository.com/artifact/org.springdoc/springdoc-openapi-ui
	implementation("org.springdoc:springdoc-openapi-ui:1.6.3")

	//https://mvnrepository.com/artifact/org.springdoc/springdoc-openapi-kotlin
	implementation("org.springdoc:springdoc-openapi-kotlin:1.6.3")

	runtimeOnly("software.amazon.awssdk:bom:2.17.107")
	testImplementation("org.springframework.boot:spring-boot-starter-test")
}

tasks.withType<KotlinCompile> {
	kotlinOptions {
		freeCompilerArgs = listOf("-Xjsr305=strict")
		jvmTarget = "11"
	}
}

tasks.withType<Test> {
	useJUnitPlatform()
}
